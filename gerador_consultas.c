/*
MIT License

Copyright (c) 2025 José Eduardo Gaboardi de Carvalho

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

*/

/*
Gera apes para consultas a banco Oracle.

gcc -Wall -Wextra -pedantic -s -static -O2 -o gerador_consultas.exe gerador_consultas.c

*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

char *source[] = {
"set serveroutput on size 100000 for wra;",
"set linesize 120;",
"declare",
"    fd utl_file.file_type;",
"    cursor cs_cursor is",
"        select",
/* "        NR_ID," */
"\xff", /* quebra */
"        1 dummy_nao_utilizado",  /* apenas para facilitar o tratamento de vírgulas */
"        from NOME_TABELA -- #TODO: Alterar!",
"        where 1 = 0 -- #TODO: Alterar!",
"    ;",
"    function fc_date(d in date) return varchar2 is",
"    begin",
"        if d is null then",
"            return null;",
"        end if;",
"        return to_char(d,'yyyy-mm-dd hh24:mi:ss');",
"    end fc_date;",
"    function fc_timestamp(t in timestamp) return varchar2 is",
"    begin",
"        if t is null then",
"            return null;",
"        end if;",
"        return to_char(t,'yyyy-mm-dd hh24:mi:ss.ff6');",
"    end fc_timestamp;",
"    function fc_number(n in number) return varchar2 is",
"    begin",
"        if n is null then",
"            return null;",
"        end if;",
"        return rtrim(trim(to_char(n,'FM9999999999999999999999999999990D999999999999999','NLS_NUMERIC_CHARACTERS = ''.,''')),'.');",
"    end fc_number;",
"    function fc_varchar2(v in varchar2) return varchar2 is",
"        v2 varchar2(32767);",
"        usar_aspas number(1);",
"    begin",
"        if v is null then return null; end if;",
"        usar_aspas := 0;",
"        if instr(v, '\"') > 0 or instr(v, ',') > 0 then usar_aspas := 1;",
"        elsif instr(v, chr(10)) > 0 or instr(v, chr(13)) > 0 then usar_aspas := 1;",
"        elsif REGEXP_INSTR(v,",
"            '^[- '||chr(33)||'#$%'||chr(38)||chr(39)||",
"            '()*+./0-9:;<=>?@A-Z[.[.]\\\\[.].]^_`a-z{|}~]*$'",
"        )<>1 then usar_aspas := 1;",
"        end if;",
"        v2 := replace(v,'\"','\"\"');",
"        if usar_aspas = 0 then return v2; end if;",
"        return '\"'||v2||'\"';",
"    end fc_varchar2;",
"    function fc_chave_tripla(a in varchar2, b in varchar2, c in varchar2) return varchar2 is",
"    begin",
"        if a is null and b is null and c is null then",
"            return null;",
"        end if;",
"        return fc_varchar2(fc_varchar2(a) || ',' || fc_varchar2(b) || ',' || fc_varchar2(c));",
"    end fc_chave_tripla;",
"begin",
"    fd := utl_file.fopen('XPTO_UTL', 'log/' || -- #TODO: Alterar local e nome!",
"\xff", /* quebra */
"        , 'W', 4001",
"    );",
"\xff", /* quebra */
"    utl_file.put_line(fd, '');",
"    for rs in cs_cursor loop",
"\xff", /* quebra */
/*
"        --utl_file.put(fd, fc_number(rs.NR_ID));"
"        --utl_file.put(fd, ',' || fc_number(rs.NR_ID));"
*/
"        utl_file.put_line(fd, '');",
"    end loop;",
"    utl_file.fclose(fd);",
"end;",
"/",
"\xff" /* fim */
};

int main (void)
{
    int i, j;
    int lin;
    int n_colunas;
    char colunas[500][31];
    int tipos[500];
    char buf[1024];
    char nomearq[128];
    char nomelog[128];
    struct tm *hora_local;
    time_t time_t_atual;
    FILE *fo;

    time_t_atual = time (NULL);
    hora_local = localtime (&time_t_atual);

    sprintf (nomearq, "apes_%04d-%02d-%02d_%02d-%02d-%02d.sql",
        hora_local->tm_year + 1900,
        hora_local->tm_mon + 1,
        hora_local->tm_mday,
        hora_local->tm_hour,
        hora_local->tm_min,
        hora_local->tm_sec
    );
    fo = fopen (nomearq, "wb");
    if (fo == NULL) {
        fprintf (stderr, "Erro ao criar arquivo %s\n.", nomearq);
        exit (1);
    }

    n_colunas = 0;
    while (fgets (buf, 1024, stdin) != NULL) {
        for (i = 0; buf[i] != '\0'; i++);
        /* rtrim */
        while (i > 0 && (buf[i - 1] == ' ' || buf[i - 1] == '\t' || buf[i - 1] == '\r' || buf[i - 1] == '\n')) i--;
        if (i == 0) continue;
        /* ltrim */
        for (i = 0; (buf[i - 1] == ' ' || buf[i - 1] == '\t' || buf[i - 1] == '\r' || buf[i - 1] == '\n'); i++) ;
        if (i > 0) {
            j = 0;
            while(buf[i] != '\0') {
                buf[j] = buf[i];
                i++;
                j++;
            }
            buf[j] = '\0';
        }
        if (buf[0] < 'A' || buf[0] > 'Z') continue; /* não é nome de coluna */
        if (buf[1] != ' ' && (buf[1] < 'A' || buf[1] > 'Z')) continue; /* é cabeçalho do DESC */
        i = 0;
        while ((buf[i + 1] >= 'A' && buf[i + 1] <= 'Z') || (buf[i + 1] >= '0' && buf[i + 1] <= '9') || buf[i + 1] == '_') i++;
        buf[i + 1] = '\0';
        strcpy (colunas[n_colunas], buf);
        i += 2;
        /* pegar tipo da coluna */
        while (buf[i] == ' ') i++;
        do {
            j = i; /* j marca o inicio da string */
            if (buf[i] < 'A' || buf[i] > 'Z') {
                j = -1;
                break;
            }
            while ((buf[i + 1] >= 'A' && buf[i + 1] <= 'Z') || (buf[i + 1] >= '0' && buf[i + 1] <= '9')) i++;
            buf[i + 1] = '\0';
            i += 2;
            while (buf[i] == ' ') i++;
        } while (strcmp (buf + j, "NOT") == 0 || strcmp (buf + j, "NULL") == 0);
        if (j == -1) continue;
        if (strcmp (buf + j, "DATE") == 0) tipos[n_colunas] = 1;
        else if (strcmp (buf + j, "NUMBER") == 0) tipos[n_colunas] = 2;
        else if (strcmp (buf + j, "VARCHAR2") == 0) tipos[n_colunas] = 3;
        else if (strcmp (buf + j, "CHAR") == 0) tipos[n_colunas] = 3;
        else if (strcmp (buf + j, "TIMESTAMP") == 0) tipos[n_colunas] = 4;
        else tipos[n_colunas] = 0;
        n_colunas++;
    }
    for (i = 0; i < n_colunas; i++) {
        if (tipos[i] == 0) {
            fprintf (stderr, "Tipo nao previsto da coluna %s\n", colunas[i]);
            fprintf (stderr, "Pressione enter...");
            fflush (stdout);
            while (getchar() != '\n');
            exit (1);
        }
    }
    for (lin = 0; (unsigned char)source[lin][0] != 0xff; lin++) fprintf (fo, "%s\n", source[lin]);
    for (i = 0; i < n_colunas; i++) fprintf (fo, "        %s,\n", colunas[i]);
    for (lin++; (unsigned char)source[lin][0] != 0xff; lin++) fprintf (fo, "%s\n", source[lin]);
    strcpy (nomelog, nomearq);
    nomelog[25] = '\0';
    strcat (nomelog, "txt");
    fprintf (fo, "        '%s'\n", nomelog);
    for (lin++; (unsigned char)source[lin][0] != 0xff; lin++) fprintf (fo, "%s\n", source[lin]);
    for (i = 0; i < n_colunas; i++) fprintf (fo, "    utl_file.put(fd, '%s%s');\n", (i != 0 ? "," : ""), colunas[i]);
    for (lin++; (unsigned char)source[lin][0] != 0xff; lin++) fprintf (fo, "%s\n", source[lin]);
    for (i = 0; i < n_colunas; i++) {
        fprintf (fo, "        utl_file.put(fd,%s", (i != 0 ? " ',' || " : " "));
        switch (tipos[i]) {
        case 1: fprintf (fo, "fc_date(rs.%s));", colunas[i]); break;
        case 2: fprintf (fo, "fc_number(rs.%s));", colunas[i]); break;
        case 3: fprintf (fo, "fc_varchar2(rs.%s));", colunas[i]); break;
        case 4: fprintf (fo, "fc_timestamp(rs.%s));", colunas[i]); break;
        }
        fprintf (fo, "\n");
    }
    for (lin++; (unsigned char)source[lin][0] != 0xff; lin++) fprintf (fo, "%s\n", source[lin]);

    fclose (fo);
    printf ("Saida gerada em %s\n", nomearq);
    printf ("Pressione Enter...");
    fflush (stdout);
    while (getchar() != '\n');
    return 0;
}

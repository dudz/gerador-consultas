# Gerador de Consultas

Gerador de Consultas é um programa para gerar código
PL/SQL para consultas de bancos Oracle e gravação em
formato CSV.


## Licença de uso


```
MIT License

Copyright (c) 2025 José Eduardo Gaboardi de Carvalho <edu.a1978@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
DEALINGS IN THE SOFTWARE.
```

## Como executar


1. Rodar o executável.
2. Fazer o DESC da tabela, e colar o conteúdo na janela do programa (do jeito que vier).
3. Finalizar com Control-Z (Windows) ou Control-D (Linux, mas precisa antes compilar o arquivo .c)
4. Teclar "Enter"
5. Renomear o arquivo gerado para o nome desejado.
6. Mudar no SELECT do programa gerado o nome da tabela e critério de acesso aos
registros (procurar por "#TODO"), com eventuais joins. Se houver mais de uma tabela,
pode acrescentar todas as colunas juntas como se fossem da mesma tabela, e depois ajustar o select.
7. Alterar o directory e o nome do arquivo de saída do UTL_FILE.
